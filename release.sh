#!/bin/sh

SERNO=`cat include/serno.h | cut -d'"' -f 2`
git archive --prefix=rizon-ircd/ --format=tar HEAD | gzip > ircd-rizon-3.1.0-$SERNO.tgz
