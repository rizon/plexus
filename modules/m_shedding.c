/*
 *  ircd-hybrid: an advanced Internet Relay Chat Daemon(ircd).
 *  m_shedding.c: Enables/disables user shedding.
 *
 *  Copyright (C) 2002 by the past and present ircd coders, and others.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 *
 *  $Id$
 */

#include "stdinc.h"
#include "channel.h"
#include "client.h"
#include "ircd.h"
#include "s_conf.h"
#include "hostmask.h"
#include "numeric.h"
#include "send.h"
#include "hash.h"
#include "s_serv.h"
#include "s_gline.h"
#include "parse.h"
#include "modules.h"
#include "event.h"
#include "irc_string.h"
#include "listener.h"
#include "handlers.h"

#define SHED_RATE_MIN 5

static int rate;

static void
user_shedding_shed(void *unused)
{
  dlink_node *ptr;
  
  DLINK_FOREACH_PREV(ptr, local_client_list.tail)
  {
      struct Client *client_p = ptr->data;

      if (!MyClient(client_p) || IsOper(client_p)) /* It could be servers */
          continue;

      sendto_one(client_p, form_str(RPL_REDIR),
                 me.name, client_p->name,
                 "irc.rizon.net", // XXX
                 client_p->localClient->listener ? client_p->localClient->listener->port : 6667);
      exit_client(client_p, &me, "Server closed connection");
      break;
  }

  eventDelete(user_shedding_shed, NULL);
}

static void
user_shedding_main(void *unused)
{
  int deviation = (rate / (3+(int) (7.0f*rand()/(RAND_MAX+1.0f))));

  eventAddIsh("user shedding shed event", user_shedding_shed, NULL, rate+deviation);
}

static void
mo_shedding(struct Client *client_p, struct Client *source_p, int parc, char **parv)
{
  if (!IsRouting(source_p))
  {
    sendto_one(source_p, form_str(ERR_NOPRIVILEGES), me.name, source_p->name);
    return;
  }

  if (irccmp(parv[1], "OFF") == 0)
  {
      eventDelete(user_shedding_main, NULL);
      eventDelete(user_shedding_shed, NULL);
      sendto_realops_flags(UMODE_ALL, L_ALL,
              "User shedding DISABLED by %s", source_p->name);
      return;
  }

  rate = atoi(parv[1]);
  if (rate < SHED_RATE_MIN)
  {
    sendto_one(source_p, ":%s NOTICE %s :Rate may not be less than %d", me.name, source_p->name, SHED_RATE_MIN);
    return;
  }
    
  sendto_realops_flags(UMODE_ALL, L_ALL,
          "User shedding ENABLED by %s (%s). Shedding interval: %d seconds", 
          source_p->name, parv[parc-1], rate);

  rate -= (rate/5);

  /* Lets not start more than one main thread in case someone tweaks the
   * parameters
   */
  eventDelete(user_shedding_main, NULL);
  eventAdd("user shedding main event", user_shedding_main, NULL, rate);
}

struct Message shedding_msgtab = {
	"SHEDDING", 0, 0, 2, 0, MFLG_SLOW, 0,
	{ m_unregistered, m_not_oper, m_ignore, m_ignore, mo_shedding, m_ignore }
};

void
_modinit(void)
{
  mod_add_cmd(&shedding_msgtab);
}

void
_moddeinit(void)
{
  mod_del_cmd(&shedding_msgtab);
  eventDelete(user_shedding_main, NULL);
  eventDelete(user_shedding_shed, NULL);
}

const char *_version = "$Revision$";

